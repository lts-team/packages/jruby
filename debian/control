Source: jruby
Section: ruby
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <nomadium@debian.org>
Build-Depends: ant-optional,
               debhelper-compat (= 12),
               default-jdk (>= 2:1.9~),
               jruby-openssl (>= 0.9.17~),
               junit4,
               libasm-java (>= 5.0),
               libbsf-java,
               libbuild-helper-maven-plugin-java,
               libbytelist-java (>= 1.0.13~),
               libdirgra-java,
               libheadius-options-java,
               libinvokebinder-java (>= 1.7~),
               libjarjar-java,
               libjcodings-java,
               libjffi-java,
               libjitescript-java,
               libjnr-constants-java (>= 0.9.5~),
               libjnr-enxio-java,
               libjnr-ffi-java (>= 2.1.1~),
               libjnr-netdb-java,
               libjnr-posix-java (>= 3.0.44~),
               libjnr-unixsocket-java (>= 0.14~),
               libjnr-x86asm-java,
               libjoda-time-java,
               libjruby-joni-java,
               libjzlib-java,
               liblivetribe-jsr223-java,
               libmaven-assembly-plugin-java (>= 3.1.0~),
               libmaven-antrun-plugin-java,
               libmaven-bundle-plugin-java,
               libmaven-dependency-plugin-java,
               libmaven-exec-plugin-java,
               libmaven-invoker-plugin-java,
               libmaven-shade-plugin-java,
               libmaven-source-plugin-java,
               libmodulator-java,
               libosgi-core-java,
               libpsych-java,
               libunsafe-fences-java,
               libunsafe-mock-java,
               libyaml-snake-java,
               locales-all,
               maven-debian-helper (>= 2.0~),
               nailgun,
               rake,
               ruby-jar-dependencies,
               ruby-json (>= 2.0.1+dfsg-4~),
               ruby-psych,
               ruby-rspec,
               ruby-test-unit
Build-Conflicts: open-infrastructure-locales-c.utf-8
Standards-Version: 4.3.0
Homepage: http://jruby.org/
Vcs-Git: https://salsa.debian.org/java-team/jruby.git
Vcs-Browser: https://salsa.debian.org/java-team/jruby

Package: jruby
Architecture: all
Depends: default-jre | java9-runtime,
         ant,
         junit4,
         libasm-java (>= 5.0),
         libbsf-java,
         libbytelist-java (>= 1.0.13~),
         libdirgra-java,
         libheadius-options-java,
         libinvokebinder-java (>= 1.7~),
         libjansi-java,
         libjcodings-java,
         libjffi-jni,
         libjitescript-java,
         libjoda-time-java,
         libjnr-constants-java (>= 0.9.5~),
         libjnr-enxio-java,
         libjnr-ffi-java (>= 2.1.1~),
         libjnr-netdb-java,
         libjnr-posix-java (>= 3.0.44~),
         libjnr-unixsocket-java (>= 0.14~),
         libjnr-x86asm-java,
         libjruby-joni-java,
         libjzlib-java,
         libmodulator-java,
         libosgi-core-java,
         libpsych-java,
         libunsafe-fences-java,
         libunsafe-mock-java,
         libyaml-snake-java,
         nailgun,
         ruby-jar-dependencies,
         ruby-psych,
         ${misc:Depends}
Recommends: jruby-openssl (>= 0.9.17~),
            ri,
            ruby-json,
            ruby-rspec,
            ruby-test-unit
Description: 100% pure-Java implementation of Ruby
 JRuby is an implementation of the ruby language using the JVM.
 .
 It aims to be a complete, correct and fast implementation of Ruby, at the
 same time as providing powerful new features such as concurrency without a
 global interpreter lock, true parallelism and tight integration to the Java
 language to allow one to use Java classes in Ruby programs and to allow
 JRuby to be embedded into a Java application.
 .
 JRuby can be used as a faster version of Ruby, it can be used to run Ruby
 on the JVM and access powerful JVM libraries such as highly tuned concurrency
 primitives, it can also be used to embed Ruby as a scripting language in your
 Java program, or many other possibilities.
